package hu.totha.java.behave.example;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class CarTest {

    private Car car;

    @Before
    public void setUp() {
        car = new Car("Mazda", 7.0);
    }

    @Test
    public void constructorShouldInitObjectProperly() {
        assertThat(car.getManufacturer(), is("Mazda"));
        assertThat(car.getKmpl(), is(7.0));
    }

    @Test
    public void shouldCalculateFuelConsumptionProperly() throws Exception {
        // when
        double actual = car.calculateConsumption(10.0);

        // then
        assertThat(actual, is(70.0));
    }
}
