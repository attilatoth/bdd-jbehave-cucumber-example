package hu.totha.java.behave.example;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class JBehaveTest {

    private ConsumptionCalculator calculator;
    private Car car;
    private List<Double> route;

    @Given("a <manufacturer> with consumption of <kmpl> l / 100 km")
    public void aCar(@Named("manufacturer") String manufacturer, @Named("kmpl") double kmpl) {
        calculator = new ConsumptionCalculator();
        car = new Car(manufacturer, kmpl);
    }

    @When("a route of $first, $second, $third is finished")
    public void aRouteIsFinished(double first, double second, double third) {
        route = Arrays.asList(first, second, third);
    }

    @Then("The total consumption should be <consumption>")
    public void consumptionCalculated(@Named("consumption") double expected) {
        double actual = calculator.calulateSumConsumption(car, route);

        assertThat(actual, is(expected));
    }
}
