package hu.totha.java.behave.example;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CucumberTest {

    private Car car;
    private ConsumptionCalculator calculator;
    private List<Double> route;

    @Given("^a (.+) with consumption of (\\d+) l / 100 km$")
    public void aCar(String manufacturer, double kmpl) {
        car = new Car(manufacturer, kmpl);
        calculator = new ConsumptionCalculator();
    }

    @When("^a route of (\\d+), (\\d+), (\\d+) is finished$")
    public void aRouteIsFinished(double first, double second, double third) {
        this.route = Arrays.asList(first, second, third);
    }

    @Then("^the total consumption should be (\\d+)$")
    public void consumptionCalculated(double expected) {
        double actual = calculator.calulateSumConsumption(car, route);
        assertThat(actual, is(expected));
    }

}
