package hu.totha.java.behave.example;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ConsumptionCalculatorTest {

    private ConsumptionCalculator calculator;

    @Mock
    private Car carMock;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void calculateConsumptionOfRoute() throws Exception {
        // given
        when(carMock.calculateConsumption(anyDouble())).thenReturn(1.0);
        List<Double> route = Arrays.asList(1.0, 10.0, 100.0);

        calculator = new ConsumptionCalculator();

        // when
        double actual = calculator.calulateSumConsumption(carMock, route);

        // then
        assertThat(actual, is(3.0));
    }

    @Test
    public void calculateConsumptionOfEmptyRoute() throws Exception {
        // given
        when(carMock.calculateConsumption(anyDouble())).thenReturn(1.0);
        List<Double> route = Arrays.asList();

        calculator = new ConsumptionCalculator();

        // when
        double actual = calculator.calulateSumConsumption(carMock, route);

        // then
        assertThat(actual, is(0.0));
    }
}
