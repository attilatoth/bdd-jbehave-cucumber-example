Given a <manufacturer> with consumption of <kmpl> l / 100 km
When a route of 1, 2, 3 is finished
Then The total consumption should be <consumption>

Examples:
|manufacturer|kmpl|consumption|
|Ford|7|84|
|Mazda|5|30|
