Feature: Car consumption

  Scenario: Calculate the fuel consumption of a car on a route
    Given a Ford with consumption of 7 l / 100 km
    When a route of 1, 2, 3 is finished
    Then the total consumption should be 84
