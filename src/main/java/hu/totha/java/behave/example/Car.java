package hu.totha.java.behave.example;

public class Car {

    private String manufacturer;
    private double kmpl;

    public Car(String manufacturer, double kmpl) {
        this.manufacturer = manufacturer;
        this.kmpl = kmpl;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public double getKmpl() {
        return kmpl;
    }

    public double calculateConsumption(double distance) {
        return kmpl * distance;
    }

    public boolean isFuelMonster() {
        return "Ford".equals(manufacturer);
    }
}
