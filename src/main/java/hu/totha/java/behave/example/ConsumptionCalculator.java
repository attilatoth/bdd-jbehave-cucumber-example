package hu.totha.java.behave.example;

import java.util.Arrays;
import java.util.List;

public class ConsumptionCalculator {

    private List<String> fuelMonsters;

    public ConsumptionCalculator() {
        fuelMonsters = Arrays.asList("Ford", "Chevrolet");
    }

    public double calulateSumConsumption(Car car, List<Double> route) {
        double sum = 0.0;
        for (Double distance : route) {
            sum += car.calculateConsumption(distance);
        }
        if (car.isFuelMonster()) {
            sum *= 2;
        }
        return sum;
    }
}
